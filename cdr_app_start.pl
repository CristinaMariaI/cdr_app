use Mojolicious::Lite;
use DBI;
use Mojo::JSON qw(encode_json);
use DateTime::Format::Strptime;
use Text::CSV;

# create a database connection
my $dbname = "/Users/cristina-mariastroescu/Desktop/Cdr_app/cdr_app.db";
my $dbuser = "";
my $dbpass = "";

my $driver= "SQLite";
my $dsn = "DBI:$driver:dbname=$dbname;";
my $dbh = DBI -> connect($dsn, $dbuser, $dbpass, { RaiseError => 1}) or die "Connection error: $DBI::errstr";


#  Retrieve a count and total duration of all calls in a specified time period
get '/callers' => sub {
    my $c = shift;
    #my $id = $c->stash('id');
    my $start_date = $c->param('start_date');
    my $end_date = $c->param('end_date');
    my $type = $c->param('type');
    my $sth;

    my $strp = DateTime::Format::Strptime->new(
        pattern => '%d/%m/%Y',
        time_zone => 'UTC',
    );

    # parse the input string into a DateTime object
    my $start_date_converted = $strp->parse_datetime($start_date);
    my $end_date_converted = $strp->parse_datetime($end_date);

    if ($end_date_converted > $start_date_converted->add(months=>1)){
         $c->render(json => {error => "Something went wrong!"}, status => 404);
         return;
    }
    # retrieve the data from the database
    if ($type) {
        $sth = $dbh->prepare('SELECT COUNT(duration) FROM cdr_app WHERE call_date >= $start_date AND call_date <= $end_date AND type=?');
        $sth->execute($start_date, $end_date, $type);
    } else {
        $sth = $dbh->prepare('SELECT COUNT(duration) FROM cdr_app WHERE call_date >= $start_date AND call_date <= $end_date');
        $sth->execute($start_date, $end_date);
    }
    
    my @callers_id;
    my $total = 0;
    while (my $caller_id = $sth->fetchrow_hashref) {
        push @callers_id, $caller_id;
        $total += $caller_id;
        push @callers_id, {'Total duration' => $total/3600}; #the total will be displayed in hours, not seconds
    }
    $sth->finish;

    if (@callers_id) {
        # render the data as an array of JSON objects
        $c->render(json => \@callers_id);
    } else {
        $c->render(json => {error => "Info not found"}, status => 404);
    }
};

# define a route for GET requests with a placeholder for the reference
get '/caller/:reference' => sub {
    my $c = shift;
    my $reference = $c->stash('reference');

    # retrieve the data from the database
    my $sth = $dbh->prepare('SELECT * FROM cdr_app WHERE reference = ?');
    $sth->execute($reference);
    my $caller_ref = $sth->fetchrow_hashref;
    $sth->finish;

    if ($caller_ref) {
        # render the data as JSON
        $c->render(json => $caller_ref);
    } else {
        $c->render(json => {error => "Info not found"}, status => 404);
    }
};

#info displayed by caller_id with params ( start_date, end_date, type)
get '/entries/:id' => sub {
    my $c = shift;
    my $id = $c->stash('id');
    my $start_date = $c->param('start_date');
    my $end_date = $c->param('end_date');
    my $type = $c->param('type');
    my $sth;

    my $strp = DateTime::Format::Strptime->new(
    pattern => '%d/%m/%Y',
    time_zone => 'UTC',
    );

# parse the input string into a DateTime object
    my $start_date_converted = $strp->parse_datetime($start_date);
    my $end_date_converted = $strp->parse_datetime($end_date);

    

    if ($end_date_converted > $start_date_converted->add(months=>1)){
         $c->render(json => {error => "Something went wrong!"}, status => 404);
         return;
    }
    # retrieve the data from the database
    if ($type) {
        $sth = $dbh->prepare('SELECT * FROM cdr_app WHERE caller_id = ? AND call_date >= $start_date AND call_date <= $end_date AND type=?');
        $sth->execute($id, $start_date, $end_date, $type);
    } else {
        $sth = $dbh->prepare('SELECT * FROM cdr_app WHERE caller_id = ? AND call_date >= $start_date AND call_date <= $end_date');
        $sth->execute($id, $start_date, $end_date);
    }
    my @callers_id;
    while (my $caller_id = $sth->fetchrow_hashref) {
        push @callers_id, $caller_id;
    }
    $sth->finish;

    if (@callers_id) {
        # render the data as an array of JSON objects
        $c->render(json => \@callers_id);
    } else {
        $c->render(json => {error => "Info not found"}, status => 404);
    }
};

#info displayed only by caller_id without query params
# get '/callers1/:id' => sub {
#     my $c = shift;
#     my $id = $c->stash('id');

#     # retrieve the data from the database
#     my $sth = $dbh->prepare('SELECT * FROM cdr_app WHERE caller_id = ?');
#     $sth->execute($id);
#     my @callers_id;
#     while (my $caller_id = $sth->fetchrow_hashref) {
#         push @callers_id, $caller_id;
#     }
#     $sth->finish;

#     if (@callers_id) {
#         # render the data as an array of JSON objects
#         $c->render(json => \@callers_id);
#     } else {
#         $c->render(json => {error => "Info not found"}, status => 404);
#     }
# };

#retrieve N most expensive calls, by caller_id
get '/calls/:id' => sub {
    my $c = shift;
    my $id = $c->stash('id');
    my $start_date = $c->param('start_date');
    my $end_date = $c->param('end_date');
    my $type = $c->param('type');
    my $value = $c->param('value');
    my $sth;
   
    my $strp = DateTime::Format::Strptime->new(
        pattern => '%d/%m/%Y',
        time_zone => 'UTC',
    );

    # parse the input string into a DateTime object
    my $start_date_converted = $strp->parse_datetime($start_date);
    my $end_date_converted = $strp->parse_datetime($end_date);

    if ($end_date_converted > $start_date_converted->add(months=>1)){
         $c->render(json => {error => "Something went wrong!"}, status => 404);
         return;
    }

    if ($type) {
        $sth = $dbh->prepare('SELECT * FROM cdr_app WHERE caller_id = ? AND call_date >= $start_date AND call_date <= $end_date AND type = ? ORDER BY cost DESC LIMIT $value');
        $sth->execute($id, $start_date, $end_date, $type, $value);
        
    } else {
     # retrieve the data from the database
    $sth = $dbh->prepare('SELECT * FROM cdr_app WHERE caller_id = ? AND call_date >= $start_date AND call_date <= $end_date ORDER BY cost DESC LIMIT $value');
    $sth->execute($id, $start_date, $end_date, $value);
    }

    my @callers_id;
        while (my $caller_id = $sth->fetchrow_hashref) {
            push @callers_id, $caller_id;
    }
    $sth->finish;

    if (@callers_id) {
        # render the data as an array of JSON objects
        $c->render(json => \@callers_id);
    } else {
        $c->render(json => {error => "Info not found"}, status => 404);
    }
};

#upload the csv file using API
post '/upload_csv' => sub {
  my $c = shift;

  my $file = $c->req->upload('csv_file');

  if ($file) {
    # save the uploaded file to the csv_files directory
    my $filename = $file->filename;
    my $path = $c->app->home->child('csv_files', $filename);
    $file->move_to($path);

    # connect to db and insert the values
    
    my $file_csv =  $path;

    open CSV, $file_csv or die $!;
    my @csv_content = <CSV>;
    close CSV;

    my $dbname = "/Users/cristina-mariastroescu/cdr_app/cdr_app.db";
    my $dbuser = "";
    my $dbpass = "";
    my $table = "cdr_app";

    my $driver= "SQLite";
    my $dsn = "DBI:$driver:dbname=$dbname;";
    my $dbh = DBI -> connect($dsn, $dbuser, $dbpass, { RaiseError => 1}) or die "Connection error: $DBI::errstr";
    my $sth;
    my $statement;

    my $field_line = shift(@csv_content);
    chomp($field_line);

    my @values;
    my $status;
    my $line;

    my $csv = Text::CSV->new();
    foreach(@csv_content){
        if($csv->parse($_)){
            @values = $csv->fields();
            $status = $csv->combine(@values);
            $line = $csv->string();
            my ($caller_id, $recipient, $call_date, $end_time, $duration, $cost, $reference, $currency, $type) = split "," , $line;
            print $caller_id;
            $statement = "INSERT INTO cdr_app VALUES(? , ?, ?, ?, ?, ?, ?, ?, ?);";
            #print $statement."\n";
            #$table($field_line)
            $sth = $dbh->prepare( $statement );
            $sth->execute($caller_id, $recipient, $call_date, $end_time, $duration, $cost, $reference, $currency, $type) or die "$! $DBI::errstr";

        }
    }

    $c->render(json => "File uploaded succesfully!", status => 200);
  }
  else {
    $c->render(json => {error => "Something went wrong. Please try again!"}, status => 404);
  }
};
app->start;