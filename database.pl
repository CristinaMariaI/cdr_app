#!/usr/bin/perl -w
use strict;
use DBI;
use Text::CSV;

my $file =  "techtest_cdr_dataset.csv";

open CSV, $file or die $!;
my @csv_content = <CSV>;
close CSV;

my $dbname = "/Users/cristina-mariastroescu/Desktop/Cdr_app/cdr_app.db";;
my $dbuser = "";
my $dbpass = "";
my $table = "cdr_app";

my $driver= "SQLite";
my $dsn = "DBI:$driver:dbname=$dbname;";
my $dbh = DBI -> connect($dsn, $dbuser, $dbpass, { RaiseError => 1}) or die "Connection error: $DBI::errstr";
my $sth;
my $statement;

my $field_line = shift(@csv_content);
chomp($field_line);

my @values;
my $status;
my $line;

my $csv = Text::CSV->new();
foreach(@csv_content){
   if($csv->parse($_)){
     @values = $csv->fields();
     $status = $csv->combine(@values);
     $line = $csv->string();
     my ($caller_id, $recipient, $call_date, $end_time, $duration, $cost, $reference, $currency, $type) = split "," , $line;
     print $caller_id;
     $statement = "INSERT INTO cdr_app VALUES(? , ?, ?, ?, ?, ?, ?, ?, ?);";
     #print $statement."\n";
     #$table($field_line)
     $sth = $dbh->prepare( $statement );
     $sth->execute($caller_id, $recipient, $call_date, $end_time, $duration, $cost, $reference, $currency, $type) or die "$! $DBI::errstr";
   

   }
}