#!/usr/bin/perl

use DBI;
use strict;

my $driver   = "SQLite";
my $database = "cdr_app.db";
my $dsn = "DBI:$driver:dbname=$database";
my $userid = "";
my $password = "";
my $dbh = DBI->connect($dsn, $userid, $password, { RaiseError => 1 })
   or die $DBI::errstr;
print "Opened database successfully\n";

my $stmt = qq(CREATE TABLE cdr_app
   (caller_id INT      NOT NULL,
      recipient           INT    NOT NULL,
      call_date            INT     NOT NULL,
      end_time        INT NOT NULL,
      duration         INT NOT NULL,
      cost             INT NOT NULL,
      reference        TEXT NOT NULL,
      currency         TEXT NOT NULL,
      type             INT NOT NULL
      ););

my $rv = $dbh->do($stmt);
if($rv < 0) {
   print $DBI::errstr;
} else {
   print "Table created successfully\n";
}
$dbh->disconnect();